# coding: utf8
"""
Projet réalisé par Nicolas Beuel, Jérôme Lechat et Raphael Wats dans le cadre du projet 5 pour le cours de projet informatique LINFO1102.
Ce projet est orienté autour de la création d'une skill alexa dont le sujet et la réalisation sont laissés aux choix des membes du groupe.

Notre skill appellée "last news" est une application permettant d'obtenir les dernières nouvelles mais également différentes informations
sur les prochains films à venir, leurs détails et autre ##INTRODUIRE ICI LE BUT DE LA SKILL DE RAF##. S'il existe déjà une application de
news développée par Amazon pour Alexa, cette dernière reste assez faible au niveau des différentes possibilitées et nous avons donc décider
d'en créer une plus complète, comprenant la possibilité d'obtenir de l'information selon plusieurs critères comme le pays, la langue, une
catégorie et/ou la langue.
"""
from flask import Flask
from flask_ask import Ask, statement, question
import request_news
import filmInfo

app = Flask(__name__)
ask = Ask(app, '/')

##
#
# comlpèter la doc en pressisant quelle fonction est appeléee par alexa et autre détails
#
##


@ask.launch
def start():
    """
    Fonction étant lancée au démarage de la skill.

    pre: -
    post: Retourne une question demandant comment le skill peut aider l'utilisateur.
    """
    print("lunch")
    return question('How can I help you ?')

@ask.intent('getInfo')
def get_info(type):
    """
    Cette fonction est detiner à aider les utilisateurs à obtenir différents types d'informations concernant la skill.

    pre:  <type> permet de savoir quel est le mot clef qui interresse l'utilisateur.
    post: Renvoie une réponse adaptée à la demande de l'utilisateur.
    """
    if type == "app" or type == "application":
        rep = \
        """
        Last news is an application that allows you to have the latest news. Many options are available such
        as the choice of language, country, number of news items to be processed. Sorting by category and/or keyword
        can also be combined with all the previous options.
        """
    elif type == "cat" or type == "category":
        rep =\
        """
        Eight categories are available : business, entertainment, general, health, science, sports and technology.
        """
    elif type == "language":
        rep=\
        """
        This application has access to articles in 13 different languages. french, english, arabic, german, spanish,
        hebrew, italian, dutch, norwegian, portuguese, russian, swedish, chinese.
        """
    elif type == "country":
        rep=\
        """
        This application has access to articles from more than 50 different countries.
        """
    else:
        pass
    return statement(rep)


@ask.intent('fastNews')
def fast_news(numb, lang, country, category, keyword):
    """
    Fast news est une fonction permettant de récupérer les dernières nouvelles selon plusieurs critères extraient par
    Alexa lors de la requete de l'utilisateur. Cette fonction ne renvoyant que les titres des news.

    pre: <numb> est le nombre d'information à renvoyer à l'utilisateur. Pour des raisons technique indépendante de notre
                implémentation, une limite est fixée à 100 informations. Si l'utilisateur en demande plus, ce nombre sera
                automatiquement ramener à 100.
         <lang> permet de filtrer les news selon une langue parmit les 13 disponibles.
         <country> permet de filtrer les news selon un pays parmit les plus de 50 disponibles.
         <category> permet de filtrer les news selon une catégorie parmit les 7 suivantes : business, entertainment, general, health, science, sports et technology.
         <keyword> permet de filtrer les news selon un mot clef.

    post : Renvoie les news récupérées selon les critères de recherche.
    """
    #print(numb, lang, country, category, keyword)
    to_ret = ""

    if numb is not None:
        numb = int(numb)
        if numb > 100:
            to_ret += "Sorry, I can't find more than 100 news..."

    query = create_url(numb, lang, country, category, keyword)
    #print(query)
    if type(query) is str:
        return statement(query)

    query.get_inf()
    titles = query.get_title()

    if len(titles) > 0:
        for i in range(len(titles)):
            to_ret += (str(i + 1) + ". " + titles[i] + ".\n")
    else:
        to_ret = "Sorry I don't have any news for this request."

    return statement(to_ret)


@ask.intent('detailsNews')
def details_news(numb, lang, country, category, keyword):
    """
    Fast news est une fonction permettant de récupérer les dernières nouvelles selon plusieurs critères extraient par
    Alexa lors de la requete de l'utilisateur. Cette fonction renvoyant les titres mais également un description des
    news récupérées.

    pre: <numb> est le nombre d'information à renvoyer à l'utilisateur. Pour des raisons technique indépendante de notre
                implémentation, une limite est fixée à 100 informations. Si l'utilisateur en demande plus, ce nombre sera
                automatiquement ramener à 100.
         <lang> permet de filtrer les news selon une langue parmit les 13 disponibles.
         <country> permet de filtrer les news selon un pays parmit les plus de 50 disponibles.
         <category> permet de filtrer les news selon une catégorie parmit les 7 suivantes : business, entertainment, general, health, science, sports et technology.
         <keyword> permet de filtrer les news selon un mot clef.

    post : Renvoie les news récupérées selon les critères de recherche.
    """
    #print(numb, lang, country, category, keyword)
    to_ret = ""

    if numb is not None:
        numb = int(numb)
        if numb > 100:
            to_ret += "Sorry, I can't find more than 100 news..."

    query = create_url(numb, lang, country, category, keyword)
    #print(query)
    if type(query) is str:
        return statement(query)

    query.get_inf()
    cont = query.get_details()

    for i in range(len(cont)):
        to_ret += (str(i + 1) + ". " + cont[i][0] + ' : ' + cont[i][1] + ".\n") #à vérif

    return statement(to_ret)


def create_url(numb, lang, country, category, keyword):
    #doc a finir
    """
    Cette fonction permet la création d'une instance de l'objet RequestNews servant à gérer chaque requètes.
    pre : En réalité les arguments si dessous sont les mêmes que ceux utilisé dans les fonctions details_news() et fast_news()

          <numb> est le nombre d'information à renvoyer à l'utilisateur. Maximum 100 sinon ramené à 100.
          <lang> permet de filtrer les news selon une langue parmit les 13 disponibles.
          <country> permet de filtrer les news selon un pays parmit les plus de 50 disponibles.
          <category> permet de filtrer les news selon une catégorie parmit 7.
          <keyword> permet de filtrer les news selon un mot clef.

    post : Renvoie une instance de l'objet RequestNews()
    """
    obj = request_news.RequestNews()

    # Ajout de la langue
    tmp = obj.set_language(lang)
    if tmp is not None:
        return tmp

    # Ajout de la catégorie
    tmp = obj.set_category(category)
    if tmp is not None:
        return tmp

    # Ajout du pays
    tmp = obj.set_country(country)
    if tmp is not None:
        return tmp

    # Ajout du mot clef
    tmp = obj.set_keyword(keyword)
    if tmp is not None:
        return tmp

    # Ajout du nombre de nouvelles
    tmp = obj.set_nb_inf(numb)
    if tmp is not None:
        return tmp

    return obj

# ------ film -------
@ask.intent('whenMovie')
def details_news(movie):
    return statement(filmInfo.look(movie))

@ask.intent('infoMovie')
def details_news(movie,kind):
    kind = kind.lower()
    if kind == "rating":
        kind = "score"
    if kind in ["plot","actors","score"]:
        return statement(filmInfo.command(movie,kind))
    else:
        return statement("I don't have this information, sorry.")

if __name__ == '__main__':
    app.run()
