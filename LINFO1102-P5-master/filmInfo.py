from urllib.request import Request, urlopen
from datetime import datetime
import json


def look(name):
    """
    Cette fonction sert à déterminer le temps avant la sortie d'un film.

    pre : <name> Le nom du film recherché.
    post : Renvoie le temps exacte avant la sortie d'un film.
    """
    url = 'https://yourcountdown.to/everything?search={}'.format(name.replace(' ', '+'))
    page = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'}))
    for line in page.readlines():
        s = line.decode('utf-8')
        if s[:12] == '<a data-src=':
            i = len(s) - 25
            url = ''
            while s[i] != '"':
                url = s[i] + url
                i -= 1
            break
    page = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'}))
    for line in page.readlines():
        s = line.decode('utf-8')
        if s[:22] == '<div class="countdown"':
            return str(datetime.strptime(s[53:72], '%Y-%m-%d %H:%M:%S') - datetime.now())[:-7]


#print(look('c'))

def map_search(name):
    """
    Fonction servant à la réalisation de command().
    Cette dernière fait un appele à l'api omdbapi.

    pre : <name> Le nom à chercher.
    post : Renvoi les datas récupérées suite à la requète.
    """
    key = 'http://www.omdbapi.com/?i=tt3896198&apikey=c35ef61e&t={}'.format(name.replace(' ', '+'))
    with urlopen(key) as response:
        info = response.read().decode('utf-8')
    data = json.loads(str(info))
    return data


def command(name, action):
    """
    Cette fonction permet de récupérer les acteurs principaux, la note/100 ou un résumé du film demandé.

    pre : <name> Le noms du film recherché
          <action> Le sujet de la question au choix parmis "actors", "score" et "plot".
    post : Renvoie une phrase interprétable par Alexa sous la forme d'une string.
    """
    rm = map_search(name)
    print('searched', name, 'found', rm['Title'])
    if action == 'plot':
        return('The plot of {} is {}'.format(rm['Title'], rm['Plot']))
    elif action == 'actors':
        return('The main actors of {} are {}'.format(rm['Title'], rm['Actors']))
    elif action == 'score':
        return('{} is rated {} currently'.format(rm['Title'], rm['Metascore']))
    else:
        return('The action : ', action, 'couldn\'t be resolve')

#command('Avatar 2', 'plot')
