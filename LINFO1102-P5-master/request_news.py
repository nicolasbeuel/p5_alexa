# coding: utf8
"""
Fichier faisant partie du projet P5 dans le cadre du cours LINFO1102 et réalisé par Nicolas Beuel, Jérôme Lechat et Raphael Wats.
"""
import json
from urllib.request import urlopen


class RequestNews:
    """
    Cette classe a pour but de géré les requètes faites à l'api 'newsapi'. Elle vérifie la validitée des paramètres, gère la requète
    et la transformation de la réponse du JSON vers un dictionnaire python. De plus elle à également pour fonction de récupéré les
    informations de ce dictionnaire pour les formaters en une string utilisable par Alexa.
    """
    def __init__(self):
        """
        Initialisation de la class RequestNews.

        pre : - (les arguments nécessitants une vérification, des fonctions set sont utilisées)
        post: -
        """
        self.dic_country = {
            "argentina": "ar",
            "australia": "au",
            "austria": "at",
            "belgium": "be",
            "brazil": "br",
            "bulgaria": "bg",
            "canada": "ca",
            "china": "cn",
            "colombia": "co",
            "cuba": "cu",
            "czech republic": "cz",
            "egypt": "eg",
            "france": "fr",
            "germany": "de",
            "greece": "gr",
            "hong kong": "hk",
            "hungary": "hu",
            "india": "in",
            "indonesia": "id",
            "ireland": "ie",
            "israel": "il",
            "italy": "it",
            "japan": "jp",
            "south korea": "kr",
            "latvia": "lv",
            "lithuania": "lt",
            "malaysia": "my",
            "mexico": "mx",
            "morocco": "ma",
            "netherlands": "nl",
            "new zealand": "nz",
            "nigeria": "ng",
            "norway": "no",
            "philippines": "ph",
            "poland": "pl",
            "portugal": "pt",
            "romania": "ro",
            "russia": "ru",
            "saudi arabia": "sa",
            "serbia": "rs",
            "singapore": "sg",
            "slovakia": "sk",
            "slovenia": "si",
            "south africa": "za",
            "sweden": "se",
            "switzerland": "ch",
            "taiwan": "tw",
            "thailand": "th",
            "turkey": "tr",
            "ukraine": "ua",
            "arab emirates": "ae",
            "united arab emirates": "ae",
            "united kingdom": "gb",
            "uk": "gb",
            "united states": "us",
            "us": "us",
            "usa": "us",
            "venezuela": "ve"
        }
        self.dic_language = {
            "french": "fr",
            "english": "en",
            "arabic": "ar",
            "german": "de",
            "spanish": "es",
            "hebrew": "he",
            "italian": "it",
            "dutch": "nl",
            "norwegian": "no",
            "portuguese": "pt",
            "russian": "ru",
            "swedish": "se",
            "chinese": "zh",
            }
        self.arrCategory = ["business", "entertainment", "general", "health", "science", "sports", "technology"]
        self.arr_arg = []
        self.base_url = "https://newsapi.org/v2/top-headlines?"
        self.url = ""
        self.key = "apiKey=7a02efdee17f4a80a191ea28e0129fd5"
        self.data = None

        self.country = None
        self.language = None
        self.nb_inf = 5
        self.category = None
        self.keyword = None

    def set_category(self, category):
        """
        Cette méthode permet de configurer une catégorie.
        Renvoie une string d'erreur si la catégorie n'est pas valide.

        pre : <category> la catégorie dans laquelle chercher.
        post : Ne renvoie une string que si la catégorie n'est pas valide.
        """

        if category is not None:
            category = category.lower()
            if category in self.arrCategory:
                self.category = category
            else:
                return "I don't have this category in my databse, sorry."

    def set_keyword(self, keyword):
        """
        Cette méthode permet de configurer un mot clef.
        Renvoie une string d'erreur si le mot clef n'est pas valide.

        pre : <keyword> le mot clef à chercher.
        post : Ne renvoie une string que si le mot clef n'est pas valide.
        """

        if keyword is not None:
            keyword = keyword.lower()
            tmp = False
            for i in keyword:
                if i not in "azertyuiopqsdfghjklmwxcvbn":
                    tmp = True
            if not tmp:
                self.keyword = keyword
            else:
                return "You can't use special character or space, sorry."

    def set_country(self, country):
        """
        Cette méthode permet de configurer un pays.
        Renvoie une string d'erreur si le pays n'est pas valide.

        pre : <country> le pays dans lequel chercher.
        post : Ne renvoie une string que si le pays n'est pas valide.
        """
        if country is not None:
            country = country.lower()
            if country in self.dic_country:
                self.country = self.dic_country[country]
            else:
                return "I don't have news from this contry, sorry."

    def set_language(self, language):
        """
        Cette méthode permet de configurer une langue.
        Renvoie une string d'erreur si la langue n'est pas valide.

        pre : <language> la langue dans laquelle chercher.
        post : Ne renvoie une string que si la langue n'est pas valide.
        """
        if language is not None:
            language = language.lower()
            if language in self.dic_language:
                self.language = self.dic_language[language]
            else:
                return "I don't have news in this language, sorry."

    def set_nb_inf(self, numb):
        """
        Cette méthode permet de configurer le nombre de news a récupérer.
        Renvoie une string d'erreur si le nombre n'est pas valide.

        pre : <numb> Le nombre de news à chercher.
        post : Ne renvoie une string que si le nombre n'est pas valide.
        """
        if numb is not None:
            numb = int(numb)
            if numb > 100:
                numb = 100
            if numb < 1:
                return "You're to drunk bro, go to sleep right now."

            self.nb_inf = numb

    def get_title(self):
        """
        Cette méthode permet de récupérer l'ensemble des titres suite à une recherche.
        Ces derniers seront formater dans un format interprétable par Alexa.

        pre: -
        post: Renvoi une string contenant les derniers titres selon la recherche faite précédement.
        """
        if self.data is None:
            self.get_inf()
        tmp_arr = []
        for i in range(len(self.data["articles"])):
            tmp_arr.append(self.data["articles"][i]["title"].split("-")[0].strip())
        return tmp_arr

    def get_details(self):
        """
        Cette méthode permet de récupérer l'ensemble des titres et détails suite à une recherche.
        Ces derniers seront formater dans un format interprétable par Alexa.

        pre: -
        post: Renvoi une string contenant les derniers titres et détails selon la recherche faite précédement.
        """
        if self.data is None:
            self.get_inf()
        tmp_arr = []
        for i in range(len(self.data["articles"])):
            tmp_arr.append([self.data["articles"][i]["title"].split("-")[0].strip(), self.data["articles"][i]["description"]])
        return tmp_arr

    def get_inf(self):
        """
        Cette méthode permet de faire une requete à NewsApi avec les parrametres utilisés précédement.

        pre:-
        post:-
        """
        if self.country is not None:
            self.arr_arg.append("country=" + self.country)
        if self.language is not None:
            self.arr_arg.append("language=" + self.language)
        if self.category is not None:
            self.arr_arg.append("category=" + self.category)
        if self.keyword is not None:
            self.arr_arg.append("q=" + self.keyword)

        if self.country is None and self.language is None:
            self.arr_arg.append("language=en")

        self.arr_arg.append("pageSize=" + str(self.nb_inf))

        for i in self.arr_arg:
            self.base_url += i + "&"

        self.url = self.base_url + self.key

        with urlopen(self.url) as response:
            info = response.read().decode('utf-8')

        self.data = json.loads(str(info))
