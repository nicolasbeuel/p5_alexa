from flask import Flask
from flask_ask import Ask, statement, question
import RequestNews

app = Flask(__name__)
ask = Ask(app, '/')

@ask.launch
def start():
    print("lunch")
    return question('Yes ?')


@ask.intent('wNews')
def news(numb,lang,country,category,keyword):
    print(numb, lang, country, category,keyword)
    test = RequestNews.RequestNews()
    to_ret = ""

    if lang != None:
        tmp = test.GetLangage(lang.lower())
        if tmp != None:
            return statement(tmp)

    if category != None:
        tmp = test.GetCategory(category.lower())
        if tmp != None:
            return statement(tmp)

    if country != None:
        tmp = test.GetCountry(country.lower())
        if tmp != None:
            return statement(tmp)

    if keyword != None:
        tmp = test.GetKeyword(keyword.lower())
        if tmp != None:
            return statement(tmp)

    if numb != None:
        numb = int(numb)
        if numb > 100:
            numb = 100
            to_ret += "Sorry, I can't find more than 20 news..."
        if numb<1:
            return statement("You're to drunk bro, go to sleep right now.")
        test.nbInf = numb

    test.getInf()
    tlt = test.getTitle()

    for i in range(len(tlt)):
        to_ret += (str(i + 1) + ". " + tlt[i] + ".\n")

    return statement(to_ret)

    

if __name__ == '__main__':
    app.run()