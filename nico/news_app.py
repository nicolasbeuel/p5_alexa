from flask import Flask
from flask_ask import Ask, statement, question
import request_news

app = Flask(__name__)
ask = Ask(app, '/')


@ask.launch
def start():
    print("lunch")
    return question('Yes ?')


@ask.intent('fastNews')
def news(numb, lang, country, category, keyword):
    print(numb, lang, country, category, keyword)
    to_ret = ""

    if numb > 100:
        to_ret += "Sorry, I can't find more than 100 news..."

    query = create_url(numb, lang, country, category, keyword)
    print(query)
    if type(query) is str:
        return statement(query)

    query.getInf()
    titles = query.getTitle()

    for i in range(len(titles)):
        to_ret += (str(i + 1) + ". " + titles[i] + ".\n")

    return statement(to_ret)


@ask.intent('detailsNews')
def news(numb, lang, country, category, keyword):
    print(numb, lang, country, category, keyword)
    to_ret = ""

    if numb > 100:
        to_ret += "Sorry, I can't find more than 100 news..."

    query = create_url(numb, lang, country, category, keyword)
    print(query)
    if type(query) is str:
        return statement(query)

    query.getInf()
    titles = query.getTitle()

    for i in range(len(titles)):
        to_ret += (str(i + 1) + ". " + titles[i] + ".\n")

    return statement(to_ret)


def create_url(numb, lang, country, category, keyword):
    obj = RequestNews.RequestNews()

    if lang is not None:
        tmp = obj.get_language(lang.lower())
        if tmp is not None:
            return tmp

    if category is not None:
        tmp = obj.get_category(category.lower())
        if tmp is not None:
            return tmp

    if country is not None:
        tmp = obj.get_country(country.lower())
        if tmp is not None:
            return tmp

    if keyword is not None:
        tmp = obj.get_keyword(keyword.lower())
        if tmp is not None:
            return tmp

    if numb is not None:
        numb = int(numb)
        if numb > 100:
            numb = 100
        if numb < 1:
            return "You're to drunk bro, go to sleep right now."
        obj.nb_inf = numb
    return obj

if __name__ == '__main__':
    app.run()
