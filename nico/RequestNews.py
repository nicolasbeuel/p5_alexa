import json
from urllib.request import urlopen

"""
    Personnal note

        https://newsapi.org/v2/top-headlines?
            -country     --> ae ar at au be bg br ca ch cn co cu cz de eg fr gb gr hk hu id ie il in it jp kr lt lv ma mx my ng nl no nz ph pl pt ro rs ru sa se sg si sk th tr tw ua us ve za
            -category    --> business entertainment general health science sports technology
            -sources
            -q
            -pageSize    --> max 100
            -page

        https://newsapi.org/v2/everything?
            -q
            -sources
            -domains
            -excludeDomains
            -from
            -to
            -language
            -sortBy
            -pageSize
            -page

        https://newsapi.org/v2/sources?
            -category
            -language
            -country

"""

class RequestNews():
    def __init__(self,language=None,country=None,category=None,keyword=None,nbInf = 5):
        self.dicCountry={"belgium":"be","france":"fr","united state":"us"} #ae ar at au be bg br ca ch cn co cu cz de eg fr gb gr hk hu id ie il in it jp kr lt lv ma mx my ng nl no nz ph pl pt ro rs ru sa se sg si sk th tr tw ua us ve za
        self.dicLangage={"french":"fr","english":"en"} #ar de en es fr he it nl no pt ru se ud zh
        self.arrCategory=["business","entertainment","general","health","science","sports","technology"]
        self.arrArg = []
        self.base_url = "https://newsapi.org/v2/top-headlines?"
        self.key = "apiKey=7a02efdee17f4a80a191ea28e0129fd5"
        self.data = None

        self.country = country
        self.language = language
        self.nbInf = nbInf if nbInf < 101 else 100 #use pageSize
        self.category = category
        self.keyword = keyword

    def GetCategory(self, category):
        if category in self.arrCategory:
            self.category = category
        else:
            return "I don't have this category in my databse, sorry."

    def GetKeyword(self, keyword):
        tmp = False
        for i in keyword:
            if i not in "azertyuiopqsdfghjklmwxcvbn":
                tmp = True
        if not tmp:
            self.keyword = keyword
        else:
            return "You can't use special character or space, sorry."

    def GetCountry(self,country):
        if country in self.dicCountry:
            self.country = self.dicCountry[country]
        else:
            return "I don't have news from this contry, sorry."

    def GetLangage(self,language):
        if language in self.dicLangage:
            self.language = self.dicLangage[language]
        else:
            return "I don't have news in this language, sorry."

    def getTitle(self):
        tmpArr = []
        for i in range(len(self.data["articles"])):
            tmpArr.append(self.data["articles"][i]["title"].split("-")[0].strip())
        return tmpArr

    def getDetails(self):
        tmpArr = []
        for i in range(self.nbInf):
            tmpArr.append([self.data["articles"][i]["title"].split("-")[0].strip(),self.data["articles"][i]["description"]])
        return tmpArr


    def getInf(self):
        if self.country != None:
            self.arrArg.append("country=" + self.country)
        if self.language != None:
            self.arrArg.append("language=" + self.language)
        if self.category != None:
            self.arrArg.append("category=" + self.category)
        if self.keyword != None:
            self.arrArg.append("q=" + self.keyword)

        if self.country == None and self.language == None:
            self.arrArg.append("language=en")

        self.arrArg.append("pageSize=" + str(self.nbInf))

        for i in self.arrArg:
            self.base_url += i + "&"

        self.url = self.base_url + self.key
        print(self.url)
        print(self.country)
        print(self.language)
        print(self.category)
        print(self.nbInf)
        print(self.keyword)



        with urlopen(self.url) as response:
            info = response.read().decode('utf-8')

        self.data = json.loads(str(info))