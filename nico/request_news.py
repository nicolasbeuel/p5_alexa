import json
from urllib.request import urlopen


class RequestNews:
    def __init__(self, language=None, country=None, category=None, keyword=None, nb_inf=5):
        self.dic_country = {"belgium": "be", "france": "fr", "united state": "us"}  # ae ar at au be bg br ca ch cn co cu cz de eg fr gb gr hk hu id ie il in it jp kr lt lv ma mx my ng nl no nz ph pl pt ro rs ru sa se sg si sk th tr tw ua us ve za
        self.dic_language = {"french": "fr", "english": "en"}  # ar de en es fr he it nl no pt ru se ud zh
        self.arrCategory = ["business", "entertainment", "general", "health", "science", "sports", "technology"]
        self.arr_arg = []
        self.base_url = "https://newsapi.org/v2/top-headlines?"
        self.url = ""
        self.key = "apiKey=7a02efdee17f4a80a191ea28e0129fd5"
        self.data = None

        self.country = country
        self.language = language
        self.nb_inf = nb_inf if nb_inf < 101 else 100
        self.category = category
        self.keyword = keyword

    def get_category(self, category):
        if category in self.arrCategory:
            self.category = category
        else:
            return "I don't have this category in my databse, sorry."

    def get_keyword(self, keyword):
        tmp = False
        for i in keyword:
            if i not in "azertyuiopqsdfghjklmwxcvbn":
                tmp = True
        if not tmp:
            self.keyword = keyword
        else:
            return "You can't use special character or space, sorry."

    def get_country(self, country):
        if country in self.dic_country:
            self.country = self.dic_country[country]
        else:
            return "I don't have news from this contry, sorry."

    def get_language(self, language):
        if language in self.dic_language:
            self.language = self.dic_language[language]
        else:
            return "I don't have news in this language, sorry."

    def get_title(self):
        tmp_arr = []
        for i in range(len(self.data["articles"])):
            tmp_arr.append(self.data["articles"][i]["title"].split("-")[0].strip())
        return tmp_arr

    def get_details(self):
        tmp_arr = []
        for i in range(self.nb_inf):
            tmp_arr.append([self.data["articles"][i]["title"].split("-")[0].strip(), self.data["articles"][i]["description"]])
        return tmp_arr

    def get_inf(self):
        if self.country is not None:
            self.arr_arg.append("country=" + self.country)
        if self.language is not None:
            self.arr_arg.append("language=" + self.language)
        if self.category is not None:
            self.arr_arg.append("category=" + self.category)
        if self.keyword is not None:
            self.arr_arg.append("q=" + self.keyword)

        if self.country is None and self.language is None:
            self.arr_arg.append("language=en")

        self.arr_arg.append("pageSize=" + str(self.nb_inf))

        for i in self.arr_arg:
            self.base_url += i + "&"

        self.url = self.base_url + self.key
        print(self.url)
        print(self.country)
        print(self.language)
        print(self.category)
        print(self.nb_inf)
        print(self.keyword)

        with urlopen(self.url) as response:
            info = response.read().decode('utf-8')

        self.data = json.loads(str(info))
